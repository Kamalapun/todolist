<?php
require_once 'function.php';
?>

<?php
session_start();
if(isset($_SESSION['email'])){
    $email =$_SESSION['email'];
    echo "hello ".$email;
    echo " <a href ='auth/logout.php'> Logout</a> ";
}else{
    header('location: auth/login.php');
}
?>

<?php
$page_no=pageno();
$limit=getlimit();
$offset = ($page_no - 1) * $limit;

?>

<!DOCTYPE html>
<html>
<head>
    <title>TO DO LIST</title>
</head>
<body>
<h2>TO DO LISTS</h2>
<form action="handler/insertTask.php" method="post">

    <input type="text" name="task"><input type="submit" value="Add">
</form>
<button><a href="index.php"> All</a></button>
<button><a href="activeTask.php">Incompleted</a></button>
<button><a href="inactive.php">Complete</a></button>
<button><a href="clearTask.php">ClearAll</a></button>

<?php $todos = select_all($offset,$limit); ?>
<?php if ($todos): ?>
    <ol>
        <?php foreach ($todos as $todo) : ?>
            <?php $GLOBALS['todo'] = $todo ?>
            <?php include "partial/single.todo.php" ?>
        <?php endforeach; ?>
    </ol>
<?php endif ?>
<?php include "partial/pagination.todo.php";?>

<div>
    <form method="get">
        <label>No of tasks per page </label>
        <select name="tasksPerPage">
            <option selected value="2">2</option>
            <option value="3">3</option>
            <option value="5">5</option>
        </select>
        <input type="submit" name="pag" value="submit">
    </form>
</div>

</body>
</html>