
<?php
require_once '../function.php';
session_start();
if (isset($_POST['updateTask']) && isset($_POST['todo_id']) && isset($_SESSION['id'])) {
    $id =$_POST['todo_id'];
    $todoText=$_POST['updateTask'];
    $user_id=$_SESSION['id'];
    update_task($id,$user_id,$todoText);
    header('Location: ' . $_SERVER['HTTP_REFERER']);
}
