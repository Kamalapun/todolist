<?php


require_once  'function.php';
if(isset($_POST['deleteTask']) && isset($_POST['todo_id']) &&isset($_SESSION['id'])){
    $id =$_POST['todo_id'];
    $user_id=$_SESSION['id'];
    delete_task($id,$user_id);
    header('Location: '. $_SERVER['HTTP_REFERER']);
}