
<li>
    <form action="#" method="post">
        <?php $todo = $GLOBALS['todo'] ;?>
        <input type="checkbox"
            <?php
            if($todo['status'] == 'checked'){
                echo 'checked="checked"';
            }
            ?> >

        <span><?= $todo['task']?></span>
        <input type="submit" name="toggleStatus" value="<?php $todo['status'] == 'checked' ? print 'checked' : print 'unchecked';?>">

        <button name="deleteTask">Delete</button>
        <a href="partial/edit.todo.php?id=<?=$todo['id'] ?>">Update</a>
        <input type="hidden" name="todo_id" value="<?= $todo['id'] ?>">
    </form>
</li>

<?php require_once 'handler/deleteTask.php';
?>
<?php require_once 'handler/toggle.php';?>
