

<?php
include_once '../function.php';
$db=getDatabaseConnection();
session_start();
$id =$_GET['id'];
$user_id=$_SESSION['id'];
$result = getsingleTask($id,$user_id);
$row =$result->fetch_array(MYSQLI_ASSOC);
//print_r($row);
$newtask = htmlspecialchars($row['task']);
?>

<!DOCTYPE html>
<html>
<body>
<h2>TO DO LIST</h2>
<form action="../handler/updateTask.php" method="post">
    <input type="text" name="updateTask" value="<?=$newtask?>">
    <input type="submit" value="update">
    <input type="hidden" name="todo_id" value="<?= $id ?>">
</form>
</body>
</html>
