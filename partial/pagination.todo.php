<?php
require_once 'function.php';
require_once 'index.php';

$db = getDatabaseConnection();
$user_id=$_SESSION['id'];
$previous_page = $page_no - 1;
$next_page = $page_no + 1;
$result_count = mysqli_query($db, "select count(*) As total_tasks from todo where user_id='$user_id'");
$total_tasks = mysqli_fetch_array($result_count);
$total_tasks = $total_tasks['total_tasks'];
if($total_tasks ==0){
    die("NO tasks!");
}

//$total_no_ofpages = ceil($total_tasks / $total_task_per_page);
$limit=getlimit();
$total_no_ofpages = (int)ceil($total_tasks / $limit);
//$result = mysqli_query($db, "select * from todo limit $offset,$total_task_per_page");


mysqli_close($db);
?>

<ul class="pagination">
    <li class='page-item' <?php if ($page_no <= 1) {echo "class='disabled'";} ?>>
        <a class='page-link' <?php
        if ($page_no > 1){
            echo "href='?page_no=$previous_page&tasksPerPage=$limit'";
        }?>> Previous </a>
    </li>
    <?php
    if ($total_no_ofpages <= 10){
        for ($i = 1 ; $i<=$total_no_ofpages;$i++){
            if ($i ==$page_no){
                echo "<li class='page-item active'><a class='page-link'> $i</a></li>";
            }
            else{
                echo "<li class='page-item'> <a class='page-link' href='?page_no=$i&tasksPerPage=$limit'> $i </a> </li>";
            }
        }
    }
    ?>
    <li class='page-item' <?php if ($page_no >= $total_no_ofpages) {echo "class='disabled'";} ?>>
        <a class='page-link' <?php
        if ($page_no < $total_no_ofpages){
            echo "href='?page_no=$next_page&tasksPerPage=$limit'";
        }?>> Next </a>
    </li>
    <?php
    if ($page_no < $total_no_ofpages){
        echo "<li class='page-item'> <a class='page-link' href='?page_no=$total_no_ofpages&tasksPerPage=$limit'>Last </a> </li>";
    }  ?>
</ul>
<strong> Page <?php echo $page_no." of " .$total_no_ofpages; ?></strong>
