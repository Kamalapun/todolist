

<html>
<head>
    <title>reCAPTCHA demo: Simple page</title>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
</head>
<body>
<form action="../auth/recaptcha.php" method="POST">
    <div class="g-recaptcha" data-sitekey="6LcqfboUAAAAAB1RhYoOqlMJjHpEfKZ-H9WO_SeK"></div>
    <br/>
    <input type="submit" value="Submit">
</form>
</body>
</html>


<?php
require_once '../function.php';
$gcaptchaResponse = getfrompost('g-recaptcha-response');

if (!$gcaptchaResponse) {
    echo 'Robot verification failed, please try again.';
}

$secret = '6LcqfboUAAAAAIMecfzOO0LwaGuyNWzdu_wgycg6';
$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $secret . '&response=' . $_POST['g-recaptcha-response']);
$responseData = json_decode($verifyResponse);

if ($responseData->success) {
    echo 'Your contact request have submitted successfully.';
} else {
    echo 'Robot Verification failed. Please try again.';
}



