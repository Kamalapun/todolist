

<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<form method="post">
    <h2>Login</h2>
    <p>Please fill in your credentials to login.</p>
    <label>Email</label>
    <input type="text" name="email" value=""><br/>
    <label>Password</label>
    <input type="text" name="password">
    <input type="submit" name="login">
    <div class="g-recaptcha" data-sitekey="6LcqfboUAAAAAB1RhYoOqlMJjHpEfKZ-H9WO_SeK"></div>
    <br/>
    <p>Don't have an account?
        <button><a href="signup.php">Sign up now</a></button>
        <button><a href="forgotpassword.php">Forgot Password</a></button></p>

</form>
<?php
require_once "../function.php";
login();
?>