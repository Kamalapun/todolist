<?php
require_once 'function.php';
?>


<h2>TO DO LISTS</h2>
<form action="handler/insertTask.php" method="post">

    <input type="text" name="task"><input type="submit" value="Add">
</form>
<button><a href="index.php"> All</a></button>
<button><a href="activeTask.php">Incompleted</a></button>
<button><a href="inactive.php">Complete</a></button>
<button><a href="clearTask.php">ClearAll</a></button>

<?php $todos = getallinactiveTasks(); ?>
<?php if ($todos): ?>
    <ul>
        <?php foreach ($todos as $todo) : ?>
            <?php $GLOBALS['todo'] = $todo ?>
            <?php include "partial/single.todo.php" ?>
        <?php endforeach; ?>
    </ul>
<?php endif ?>

</form>
