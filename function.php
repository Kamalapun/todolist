<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

function getDatabaseConnection(): mysqli
{
    $credentials =
        [
            'host' => "localhost",
            'user' => "root",
            'password' => "root",
            'db' => "todolist"
        ];
    $db = new mysqli($credentials['host'], $credentials['user'], $credentials['password'], $credentials['db']);
    return $db;
}

function insertTodo(string $todoText)
{
    session_start();
    $db = getDatabaseConnection();
    //if(1 == 1) { xdebug_break(); }

    $user_id=$_SESSION['id'];

    $query = "INSERT INTO todo VALUES(NULL ,'$user_id','$todoText',DEFAULT)";
    $result = $db->query($query);
    if (!$result) echo "INSERT failed!";
}

function get_posts($conn, $var)
{
    return $conn->real_escape_string($_POST[$var]);
}

function select_all($offset,$limit)
{


    $user_id=$_SESSION['id'];
    $db = getDatabaseConnection();

        //$limit = get_posts($db, 'dropdown');
        $query = "select * from  todo LIMIT $limit offset $offset";
        $result = $db->query($query);
        return $result;

    //else {
//        $query = "select * from todo where user_id='$user_id'";
//        $result = $db->query($query);
//        if (!$result) die("Database access failed");
//        return $result;
//    }



}
function pageno(){
    if (isset($_GET['page_no']) && $_GET['page_no'] != "" && $_GET['tasksPerPage']) {
        $page_no = $_GET['page_no'];
    } else {
        $page_no = 1;
    }
    return $page_no;
}

function countno_ofTask(){
    $db=getDatabaseConnection();
    $user_id=$_SESSION['id'];
    $query="select count(*)  as taskcount from todo where user_id='$user_id'";
    $result=$db->query($query);
    $row=$result->fetch_array(MYSQLI_ASSOC);

    $no_of_task=$row['taskcount'];//total no of task
   return $no_of_task;
}

function getlimit(){
    if(!getfromget('tasksPerPage')){
        $limit=countno_ofTask();
        return $limit;
    }
    $limit=getfromget('tasksPerPage');
    return $limit;
}

function delete_task(int $id,$user_id)
{
    //$user_id =$_SESSION['user_id'];
    $db = getDatabaseConnection();
    $query = "delete from todo where id='$id' && user_id ='$user_id'";
    $result = $db->query($query);
    if (!$result) echo "Delete Failed.<br>";

}


function toggle_status(string $status, int $id)
{
    $db = getDatabaseConnection();
    if ($status === 'unchecked') {
        $query = "update todo set status='checked' where id='$id'";
    } else {
        $query = "update todo set status= 'unchecked' where id='$id'";
    }
    $db->query($query);

}


function update_task(int $id,int $user_id, string $todoText)
{
//    session_start();
//    $user_id=$_SESSION['user_id'];
    $db = getDatabaseConnection();
    $query = "update todo set task='$todoText' where id='$id' && user_id='$user_id'";
    $result = $db->query($query);
    if (!$result) echo "Update failed.";
}

function getallactiveTasks()
{
    session_start();
    $db = getDatabaseConnection();
    $user_id=$_SESSION['id'];
    $query = "select * from todo where user_id='$user_id' && status='unchecked'";
    $result = $db->query($query);
    return $result;

}

function getallinactiveTasks()
{
    session_start();
    $db = getDatabaseConnection();
    $user_id=$_SESSION['id'];
    $query = "select * from todo where user_id='$user_id' && status='checked'";
    $result = $db->query($query);
    return $result;
}

function clearallcompletedtask()
{
    session_start();
    $db = getDatabaseConnection();
    $user_id=$_SESSION['id'];
    $query = "delete from todo where user_id='$user_id' && status ='checked'";
    $result = $db->query($query);
    return $result;
}

function getsingleTask(int $id,int $user_id)
{
    $db = getDatabaseConnection();
//    $user_id=$_SESSION['id'];
    $query = "select task from todo where id='$id' && user_id='$user_id'";
    $result = $db->query($query);
    if (!$result) echo "No active tasks";
    return $result;
}

function signup()
{

    $db = getDatabaseConnection();
    if ($db->connect_error) die("Fatal Error");
    if (isset($_POST['signup'])) {
        $username = $_POST['username'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        $cpassword = $_POST['cpassword'];

        if ($password !== $cpassword) {
            die ("password dont match");
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            die ('Invalid email');
        }
        recaptcha();

        $token = getuniqueToken();

//        $token = bin2hex(random_bytes(60));
        $hash = password_hash($password, PASSWORD_DEFAULT);

        $query = "insert into userss values(null,'$username','$email','$hash','$token',DEFAULT)";
        $result = $db->query($query);
        if (!$result) echo "Error";
        mailsending($token);


    }
}

function getrandomtoken()
{
    $token = bin2hex(random_bytes(60));
    return $token;
}

function login()
{
    $db = getDatabaseConnection();
    if (isset($_POST['login'])) {
        $email = get_posts($db, 'email');
        $password = get_posts($db, 'password');
        if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
            die("Invalid email");
        }

        $query = "select * from userss where email ='$email'";
        $result = $db->query($query);
        $row = $result->fetch_array(MYSQLI_ASSOC);
        if (!$result) die("User not found.");
        elseif ($result->num_rows) {
            $result->close();
            if (!password_verify($password, $row['password'])) {
                die("invalid password");
            }
            if ($row['activated'] == 1) {
                recaptcha();
                session_start();
                $_SESSION['email'] = $row['email'];
                $_SESSION['id']=$row['id'];
                header('Location: ../index.php');

            }
            die ("please verify your email.");
            include_once 'partial/resendforpassword.todo.php';
                //echo htmlspecialchars("$row[email] :hi $row[email],you are now logged in as '$row[username]'");





        }
    }
}

function mailsending($token)
{

// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function


    require_once "phpMailer/Exception.php";
    require_once "phpMailer/PHPMailer.php";
    require_once "phpMailer/SMTP.php";

// Instantiation and passing `true` enables exceptions
    $mail = new PHPMailer(true);

    try {
        //Server settings
//    $mail->SMTPDebug = 2;                                       // Enable verbose debug output
        $mail->isSMTP();                                            // Set mailer to use SMTP
        $mail->Host = 'smtp.mailtrap.io';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                                   // Enable SMTP authentication
        $mail->Username = '5d46badc557eca';                     // SMTP username
        $mail->Password = 'eef81611b83164';                               // SMTP password
        $mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to

        //Recipients
        $mail->setFrom('from@example.com', 'Mailer');
        $mail->addAddress('joe@example.net', 'Joe User');     // Add a recipient
//    $mail->addAddress('ellen@example.com');               // Name is optional
        $mail->addReplyTo('info@example.com', 'Information');
//    $mail->addCC('cc@example.com');
//    $mail->addBCC('bcc@example.com');

        // Attachments
        //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

        // Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'Here is the subject';
        $mail->Body = ' Thanks for signing up!
Your account has been created, you can login automatically after you have activated your account by pressing the url below.
Please click this link to activate your account:
<a href ="http://127.0.0.1/Practicephp/todoList/auth/activate.php?token=' . $token . '">Verify</a>';
//    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        $mail->send();
        echo 'Message has been sent';
    } catch (Exception $e) {
        echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
    }

}


function forgetpassword()
{

    $db = getDatabaseConnection();
    session_start();
    if (isset($_SESSION['email'])) {
        echo "Your are already login..Please logout to reset";
    } else {
        if (isset($_POST['email']) && !empty($_POST['email'])) {
            $email = get_posts($db, 'email');
            //$token = getuniqueToken();
            $query = "select * from userss where email='$email'";
            $result = $db->query($query);
           recaptcha();
            $row = $result->fetch_array(MYSQLI_ASSOC);
            if ($row['activated'] != 1) {
                echo "Your account is not activated.please activate your account.Check your account for account verification!!";

                include 'partial/resendform.todo.php';
                return;

                //mailsending($token);
            } else {
                //include 'partial/resetform.todo.php';
                echo "Verify mail to reset password";
                include_once 'partial/resendforpassword.todo.php';
            }
        }
    }
}


function mailsendingforpasswordreset($token)
{

// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function


    require_once "phpMailer/Exception.php";
    require_once "phpMailer/PHPMailer.php";
    require_once "phpMailer/SMTP.php";

// Instantiation and passing `true` enables exceptions
    $mail = new PHPMailer(true);

    try {
        //Server settings
//    $mail->SMTPDebug = 2;                                       // Enable verbose debug output
        $mail->isSMTP();                                            // Set mailer to use SMTP
        $mail->Host = 'smtp.mailtrap.io';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                                   // Enable SMTP authentication
        $mail->Username = '5d46badc557eca';                     // SMTP username
        $mail->Password = 'eef81611b83164';                               // SMTP password
        $mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to

        //Recipients
        $mail->setFrom('from@example.com', 'Mailer');
        $mail->addAddress('joe@example.net', 'Joe User');     // Add a recipient
//    $mail->addAddress('ellen@example.com');               // Name is optional
        $mail->addReplyTo('info@example.com', 'Information');
//    $mail->addCC('cc@example.com');
//    $mail->addBCC('bcc@example.com');

        // Attachments
        //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

        // Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'Here is the subject';
        $mail->Body = ' Thanks for resetting!
Your account has been reset, you can login automatically after you have activated your account by pressing the url below.
Please click this link to activate your account:
<a href ="http://localhost/Practicephp/todoList/partial/resetform.todo.php?token=' . $token . '">Verify</a>';
//    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        $mail->send();
        echo 'Message has been sent';
    } catch (Exception $e) {
        echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
    }

}


function getfrompost($key, $default = NULL)
{
    return isset($_POST[$key]) && !empty($_POST[$key]) ? $_POST[$key] : $default;
}

function getfromget($key, $default = NULL)
{
    return isset($_GET[$key]) && !empty($_GET[$key]) ? $_GET[$key] : $default;
}

function getuniqueToken()
{
    $token = getrandomtoken();

    while (true) {
        $db = getDatabaseConnection();
        $query = "select count(*) as count from userss where token='$token'";
        $result = $db->query($query);
        $rows = $result->fetch_array(MYSQLI_ASSOC);
        $existindb = $rows['count'];
        if (!$existindb) {
            break;
        }
        $token = getrandomtoken();
    }
    return $token;
}


function getuniqueTokenforforgetpassword()
{
    $token = getrandomtoken();

    while (true) {
        $db = getDatabaseConnection();
        $query = "select count(*) as count from forgetpassword where reset_token='$token'";
        $result = $db->query($query);
        $rows = $result->fetch_array(MYSQLI_ASSOC);
        $existindb = $rows['count'];
        if (!$existindb) {
            break;
        }
        $token = getrandomtoken();
    }
    return $token;
}


function recaptcha()
{
    $gcaptchaResponse = getfrompost('g-recaptcha-response');

    if (!$gcaptchaResponse) {
        die ('Robot verification failed,please try again.');
    }

    $secret = '6LcqfboUAAAAAIMecfzOO0LwaGuyNWzdu_wgycg6';
    $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $secret . '&response=' . $_POST['g-recaptcha-response']);
    $responseData = json_decode($verifyResponse);

    if ($responseData->success) {
        //echo 'Your contact request have submitted successfully.';
    } else {
        die ('Robot Verification failed. Please try again.');
    }
}


//function recaptchasignup()
//{
//    $gcaptchaResponse = getfrompost('g-recaptcha-response');
//
//
//    if (!$gcaptchaResponse) {
//       die ('Robot verification failed,please try again.');
//    }
//
//    $secret = '6LcqfboUAAAAAIMecfzOO0LwaGuyNWzdu_wgycg6';
//    $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $secret . '&response=' . $_POST['g-recaptcha-response']);
//    $responseData = json_decode($verifyResponse);
//
//    if ($responseData->success) {
//        //echo 'Your contact request have submitted successfully.';
//    } else {
//        die ('Robot Verification failed. Please try again.');
//    }
//}

